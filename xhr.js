
function request(url, callback) {
    var xhr = new XMLHttpRequest();
    try {
        xhr.onreadystatechange = function(){
            if (xhr.readyState != 4)
                return;

            if (xhr.responseText) {
                callback(xhr.responseText);
            }
            else {
                console.debug("No response");
            }
        }

        xhr.onerror = function(error) {
            console.debug(error);
        }

        xhr.open("GET", url, true);
        xhr.send();
    } catch(e) {
        console.error(e);
    }
}

