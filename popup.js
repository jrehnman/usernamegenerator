
var dicts =
    { "adjective": "words/index.adj"
    , "adverb": "words/index.adv"
    , "noun": "words/index.noun"
    , "verb": "words/index.verb"
    }

var schemes = 
    [ ["noun"]
    , ["The", "noun"]
    , ["adjective", "noun"]
    , ["noun", "The", "adjective"]
    ]

var firstWordPattern = /^\S+/

function randomElement(list)
{
    var index = Math.floor(Math.random() * list.length);
    return list[index];
}

function lookupDict(wordlist)
{
    var words = wordlist.split("\n");
    var w;
    
    do
    {
        w = firstWordPattern.exec(randomElement(words));
    } while (w.length < 1);

    return capitalizeWords(w[0].toLowerCase().replace(/[^a-z0-9_]/g, ""));
}

function randomWord(key, callback)
{
    if (key in dicts)
    {
        request(dicts[key], function(wl) {callback(lookupDict(wl))});
    }
    else
    {
        callback(key);
    }
}

function capitalize(word)
{
    return word[0].toUpperCase() + word.slice(1);
}

function capitalizeWords(word)
{
    return word.split("_").map(capitalize).join("");
}

function generateName(callback)
{
    var scheme = randomElement(schemes);
    var name = [];

    function accum(i, word)
    {
        name[i] = word;

        // Check if all instances of accum have been called
        for (var j = 0; j < scheme.length; j++)
        {
            if ((name.length < scheme.length) || (name[j] == null))
            {
                return;
            }
        }

        // Callback with full name
        callback(name.join(""));
    }

    for (var i = 0; i < scheme.length; i++)
    {
        var dict = scheme[i];
        name.push(null);
        randomWord(dict, accum.bind(null, i));
    }
}

function printName(name)
{
    var content = document.getElementById("Content");
    
    content.innerHTML = content.innerHTML + "<span>" + name + "</span></br>";
}

for (var i = 0; i < 20; i++)
{
    generateName(printName);
}

